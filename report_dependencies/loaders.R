#### Setup ####

# Load libraries
library(tidyverse)
library(data.table)
library(beepr)
library(gt)
library(readr)
library(scales)
library(gridExtra)
# Load CCAO libraries
library(ccao)
library(assessr)
# Load libraries needed for adding tooltips
library(htmltools)
library(bsplus)
# Load leaflet library. For neighborhood map.
library(leaflet)
library(sf)
# Load disk.frame for operations on medium objects. 
library(disk.frame)
# Load renv for package managment and reproducibility 
library(renv)

# Load all R scripts from dependencies
# Filter out loaders.R so that we don't load recursively
list.files("report_dependencies", pattern = "*.R", full.names = TRUE) %>%
  .[!str_detect(., "loaders")] %>%
  sapply(source)

