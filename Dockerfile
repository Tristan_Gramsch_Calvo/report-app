# Retrieve the parent image
FROM rocker/shiny:4.0.5

# Maybe this argument is not necessary
# I believe R reads the current directory as a path. 
WORKDIR /home/app_daemon

# RUN commands on the docker daemon.
# Get and update Linux packages
RUN apt-get update && apt-get install -y \
# Transfer protocol package
libcurl4-gnutls-dev \
# Crypthografic protocols
    libssl-dev

# Install R packages
# TODO add the package version for stability
RUN R -e "install.packages('shiny')" 
RUN R -e "install.packages('htmltools')"
RUN R -e "install.packages('rvest')"
RUN R -e "install.packages('shinycssloaders')"

# We can create a shiny-server executable bundle. But we opt for the simpler argument.
# Copy the R script containing the app and the folder with the reports.
COPY ./app/app.R ./
COPY ./app/rendered_reports ./rendered_reports

# I believe this argument is not necessary. Port configuration is set in the runApp argument below. 
EXPOSE 3838

# Run an R command. 
CMD ["R", "-e", "shiny::runApp('/home/app_daemon', host = '0.0.0.0', port = 3838)"]

# Access the daemon shell: docker exec --interactive --tty <container_id> /bin/bash
# Unix executables, like shiny-server, usually live on bin
# Run the container using ports 3838. Shiny seems to handle those better. 
